package org.gcube.data.transfer.plugins.sis;

import java.io.File;
import java.io.FilenameFilter;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Set;

import org.gcube.data.transfer.library.DataTransferClient;
import org.gcube.data.transfer.library.TransferResult;
import org.gcube.data.transfer.library.faults.DestinationNotSetException;
import org.gcube.data.transfer.library.faults.FailedTransferException;
import org.gcube.data.transfer.library.faults.InitializationException;
import org.gcube.data.transfer.library.faults.InvalidDestinationException;
import org.gcube.data.transfer.library.faults.InvalidSourceException;
import org.gcube.data.transfer.library.faults.SourceNotSetException;
import org.gcube.data.transfer.model.Destination;
import org.gcube.data.transfer.model.DestinationClashPolicy;
import org.gcube.data.transfer.model.PluginInvocation;
import org.gcube.data.transfer.plugins.thredds.sis.SISPluginFactory;


public class PublishTest {

	public static void main(String[] args) throws MalformedURLException, Exception, InvalidSourceException, SourceNotSetException, FailedTransferException, InitializationException, InvalidDestinationException, DestinationNotSetException {

		TokenSetter.set("/pred4s/preprod/preVRE");
		
		
		
		String hostname="";
				// Not yet released sdi refactoring
		// new URL(SDIAbstractPlugin.management().build().getConfiguration().getByEngine(Engine.TH_ENGINE).get(0).getBaseEndpoint()).getHost();
		
		DataTransferClient client=DataTransferClient.getInstanceByEndpoint("https://"+hostname);
		
		
		File metadata=Paths.get("src/test/resources/toPublishMeta").toFile();
//		for(File f:metadata.listFiles(new FilenameFilter() {			
//			@Override
//			public boolean accept(File dir, String name) {
//				return name.endsWith(".xml");
//			}
//		})) {
//			for(File f:new File[] {
//					new File("/Users/FabioISTI/Downloads/NASA_Surface_Air_Temperature_1950_2100_rcp45.nc")
//			}) {
		
			for(URL url:new URL[] {
					new URL("https://thredds.d4science.org/thredds/fileServer/public/netcdf/ClimateChange/NASA_Precipitations_1950_2100_rcp45.nc")
			}) {
			
			// if not present, generate with sis/geotk
			Destination toSetDestination=new Destination();
			toSetDestination.setCreateSubfolders(true);
			toSetDestination.setDestinationFileName("another_dataset");
			toSetDestination.setOnExistingFileName(DestinationClashPolicy.REWRITE);
			toSetDestination.setOnExistingSubFolder(DestinationClashPolicy.APPEND);
			toSetDestination.setPersistenceId("thredds");

			//NB ITEM IS SUPPOSED TO HAVE REMOTE PATH 
			String fileLocation="public/netcdf/test_"+System.currentTimeMillis();
			toSetDestination.setSubFolder(fileLocation);
			
			PluginInvocation inv=new PluginInvocation(SISPluginFactory.PLUGIN_ID);
			inv.setParameters(Collections.singletonMap(SISPluginFactory.VALIDATE_PARAMETER, "false"));
			
			System.out.println(
//					client.localFile(f, toSetDestination, Collections.singleton(new PluginInvocation(SISPluginFactory.PLUGIN_ID))));
					client.httpSource(url, toSetDestination, inv));
					
		}

	}

	
	
}
