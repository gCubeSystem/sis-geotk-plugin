package org.gcube.data.transfer.plugins.sis;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.JAXBException;

import org.apache.sis.storage.DataStoreException;
import org.apache.sis.storage.UnsupportedStorageException;
import org.gcube.data.transfer.plugins.thredds.ISOMetadataMarshalling;
import org.gcube.data.transfer.plugins.thredds.sis.SisPlugin;

import ucar.nc2.NetcdfFile;
import ucar.nc2.dataset.NetcdfDataset;

public class TestGetMetadata {

	public static void main(String[] args) throws UnsupportedStorageException, DataStoreException, IOException, NoSuchAlgorithmException, JAXBException {
		String[] toCheckFiles=new String[] {
				
				
				// ********** HDF - NETCDF 4
				"/Users/FabioISTI/Downloads/NASA_Surface_Air_Temperature_1950_2100_rcp45.nc",
				
//				"https://thredds.d4science.org/thredds/fileServer/public/netcdf/ClimateChange/NASA_Precipitations_1950_2100_rcp45.nc",
//				"https://thredds.d4science.org/thredds/fileServer/public/netcdf/ClimateChange/NASA_Precipitations_1950_2100_rcp85.nc",
//				"https://thredds.d4science.org/thredds/fileServer/public/netcdf/ClimateChange/NASA_Surface_Air_Temperature_1950_2100_rcp45.nc",
//				"https://thredds.d4science.org/thredds/fileServer/public/netcdf/ClimateChange/NASA_Surface_Air_Temperature_1950_2100_rcp85.nc",
//				"https://thredds.d4science.org/thredds/fileServer/public/netcdf/ClimateChange/hcaf_v6_1950_2100.nc",
				
				
				// *********** NETCDF 3
				
				"/Users/FabioISTI/Downloads/Aaptos_aaptos.nc",
				"/Users/FabioISTI/Downloads/dataset-armor-3d-rep-monthly_20181115T1200Z_P20190301T0000Z.nc",
				//"/Users/FabioISTI/Downloads/data_retro_Run_10026_911.nc",
				//"/Users/FabioISTI/Downloads/2BPR6_2021_a_NRT_Gosud_V3.nc",
				"/Users/FabioISTI/Downloads/GO_WTEP_2015_TRAJ.nc"
				
		};
		
		for(String f:toCheckFiles) {
			System.out.println("checking "+f);
			try{
				check(false,f);
			}catch(Exception e) {
				System.err.println(e.getMessage());
				System.out.println("Trying opening "+f+" manually");
				open(f);
			}
		}
		System.out.println("Done");
	}

	
	private static void open(String path) throws IOException, UnsupportedStorageException, DataStoreException {
		System.out.println("Opening "+path);
		NetcdfFile f=NetcdfFile.open(path);
		System.out.println(f.getFileTypeDescription()+"\t"+f.getFileTypeId());
		
		
		NetcdfDataset ncDs=NetcdfDataset.openDataset(path);
//		NcMLReaderNew.
//		
		System.out.println(ncDs.getFileTypeDescription()+"\t"+ncDs.getFileTypeId());
		
//		NetcdfDatasets.openFile(path,null);
		
		
	}
	
	
	private static void check(boolean checkCopy,String src) throws UnsupportedStorageException, DataStoreException, IOException, NoSuchAlgorithmException, JAXBException {

		File original=new File(src);
		
		File dataset=original;
		
		if(checkCopy) {
			File temp=File.createTempFile("temp", ".temp");
			File copied=File.createTempFile("copied", ".nc");			
			transferStream(new FileInputStream(original),new FileOutputStream(temp));
			System.out.println("copied. Moving..");	

			System.out.println("Checksum original : "+getChecksum(original));
			System.out.println("Checksum temp : "+getChecksum(temp));



			Files.move(temp.toPath(), copied.toPath(),StandardCopyOption.ATOMIC_MOVE,StandardCopyOption.REPLACE_EXISTING);
			if(copied.length()<original.length()) throw new RuntimeException("Different size after moving");
			
			dataset=copied;
			
		}


		if(dataset.exists())
			System.out.println(
					ISOMetadataMarshalling.asXML(SisPlugin.getMetaFromFile(dataset)).getAbsolutePath());
		else {
			System.out.println(
					ISOMetadataMarshalling.asXML(SisPlugin.getMetaFromFile(new URL(src))).getAbsolutePath());
		}
		

	}

	
	

	private static String getChecksum(File datafile) throws NoSuchAlgorithmException, IOException {
		MessageDigest md = MessageDigest.getInstance("SHA1");
		FileInputStream fis = new FileInputStream(datafile);
		byte[] dataBytes = new byte[1024];

		int nread = 0;

		while ((nread = fis.read(dataBytes)) != -1) {
			md.update(dataBytes, 0, nread);
		};

		byte[] mdbytes = md.digest();

		//convert the byte to hex format
		StringBuffer sb = new StringBuffer("");
		for (int i = 0; i < mdbytes.length; i++) {
			sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
		}

		return sb.toString();
	}


	private static void transferStream(InputStream in, OutputStream out){

		long receivedTotal=0l;

		try{
			byte[] internalBuf=new byte[1024];
			int received=0;
			while ((received=in.read(internalBuf))!=-1){
				out.write(internalBuf,0,received);
				receivedTotal+=received;

			}
			out.flush();
		}catch(IOException e){			
			throw new RuntimeException("Unable to read from source.");
		}		
	}

}
