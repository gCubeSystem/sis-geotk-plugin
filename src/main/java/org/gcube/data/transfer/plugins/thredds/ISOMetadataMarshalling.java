package org.gcube.data.transfer.plugins.thredds;

import static java.util.Collections.singletonMap;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamResult;

import org.apache.sis.xml.MarshallerPool;
import org.apache.sis.xml.XML;
import org.opengis.metadata.Metadata;

import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ISOMetadataMarshalling {

	
	private static MarshallerPool pool=null;
	
//	static {
//		
//		File temp=new File(System.getProperty("java.io.tmpdir")+"/EPSG");
//		temp.mkdirs();
//		
//		EmbeddedConnectionPoolDataSource ds = new EmbeddedConnectionPoolDataSource();
//		ds.setConnectionAttributes("create=true");
//		
//		ds.setDatabaseName(temp.getAbsolutePath()+"/SpatialMetadata");
//		
////		ds.setDataSourceName(arg0);
////		  ds.setServerName("someHost");
////		  ds.setPortNumber("1527");
////		  ds.setDatabaseName("someDB");
//		
//		Configuration.current().setDatabase(new Supplier<DataSource>() {
//			@Override
//			public DataSource get() {
//				return ds;
//			}
//		});
//		
//		
//	}
	
	
	
	@Synchronized
	private static MarshallerPool getPool() throws JAXBException {
		if(pool==null)
			pool=new MarshallerPool(
					singletonMap(XML.METADATA_VERSION,"2007"));
		
		return pool;
	}
	
	public static final File asXML(Metadata meta) throws JAXBException, IOException {
		File tmp=File.createTempFile("tmp_meta_", ".xml");
		try (FileWriter fw=new FileWriter(tmp)){

			getPool().acquireMarshaller().marshal(meta,new StreamResult(fw));
			
//			XML.marshal(meta, new StreamResult(fw), singletonMap(XML.METADATA_VERSION,"2007"));
		}
		return tmp;		
	}
	
	
	
}
