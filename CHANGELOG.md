This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.data.transfer.sis-geotk-plugin

## [v1.3.1] 2021-03-25
Fixes [#20760](https://support.d4science.org/issues/20760) https endpoints


## [v1.3.0] 2021-03-25
Use of EPSG Dataset
Proxy support

## [v1.2.0] 2020-09-07
Upgrade to apache-sis 1.0

## [v1.1.3] 2020-09-07

### Fixes

- Integration with gcube distribution (https://support.d4science.org/issues/19612)
